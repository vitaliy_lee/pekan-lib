LABEL 	= "pekan_test"
PIN		= "0000"
SO_PIN	= "1111"

release:
	@cargo build --release

build:
	@cargo build

install:
# 	@cp target/debug/libpekan_lib.dylib /usr/local/lib
# 	@cp target/release/libpekan_lib.dylib /usr/local/lib
	@cp include/pekan.h /usr/include
	@cp target/debug/libpekan.so /usr/lib

test_token:
	@softhsm2-util --init-token --label $(LABEL) --pin $(PIN) --so-pin $(SO_PIN) --free

remove_test_token:
	@softhsm2-util --delete-token --token $(LABEL)
