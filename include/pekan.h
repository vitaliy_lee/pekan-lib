#ifndef __pekan__included__
#define __pekan__included__

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
	SUCCESS			= 0,
	KEYPAIR_ERROR	= 1,
} PekanError;

typedef enum {
	SHA256	= 0,
	SHA512	= 1,
} PekanDigestAlgorithm;

struct ByteBuffer {
	int64_t		len;
	uint8_t*	data;
};

struct ExternError {
	PekanError	code;
	char*		message; /* note: nullable */
};

extern void pekan_bytebuffer_free(struct ByteBuffer buffer);
extern void pekan_string_free(char* str);

typedef struct {} PekanCSP;
typedef struct {} PekanSigner;
typedef struct {} PekanVerifier;

typedef struct {
	char* library;
    char* label;
    char* pin;
} PekanOptions;

extern uint64_t pekan_init_csp(PekanOptions options,
								PekanCSP** csp,
								const struct ExternError* err);

extern uint64_t pekan_hash(const PekanCSP* csp,
						 	const struct ByteBuffer* const message,
						 	PekanDigestAlgorithm algorithm,
						 	const struct ByteBuffer* hash,
							const struct ExternError* err);

extern uint64_t pekan_new_signer(const PekanCSP* const csp,
								const struct ByteBuffer* const ski,
								PekanDigestAlgorithm algorithm,
								PekanSigner** signer,
                        		const struct ExternError* err);

extern uint64_t pekan_signer_sign(const PekanSigner* const signer,
								const struct ByteBuffer* const message,	
								const struct ByteBuffer* signature,
                        		const struct ExternError* err);

extern uint64_t pekan_new_verifier(const PekanCSP* const csp,
									const struct ByteBuffer* const ski,
									PekanDigestAlgorithm algorithm,
									PekanVerifier** verifier,
                        			const struct ExternError* err);

extern uint64_t pekan_verifier_verify(const PekanVerifier* const verifier,
									const struct ByteBuffer* const signature,
									const struct ByteBuffer* const message,
                        			const struct ExternError* err);

extern uint64_t pekan_finalize(PekanCSP* csp);

#ifdef __cplusplus
}
#endif

#endif
