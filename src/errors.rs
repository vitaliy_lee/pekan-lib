pub const PATH_ERROR: i32 = 0;
pub const CONFIG_ERROR: i32 = 1;
pub const PARAM_ERROR: i32 = 2;
pub const DIGEST_ERROR: i32 = 3;
pub const SIGN_ERROR: i32 = 3;
pub const VERIFY_ERROR: i32 = 4;
pub const FINALIZE_ERROR: i32 = 5;
