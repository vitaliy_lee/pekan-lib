#![allow(clippy::needless_return)]

use std::ffi::CStr;
use std::marker::{PhantomData, PhantomPinned};

use libc::c_char;

use ffi_support::{ByteBuffer, ExternError};

use fcrypto::{DigestAlgorithm, Signer, Verifier, CSP};

#[macro_use]
extern crate ffi_support;

use crate::support::{ByteArray, ErrorCode};

mod errors;
pub mod support;

#[repr(C)]
pub struct Options {
    library: *const c_char,
    label: *const c_char,
    pin: *const c_char,
}

#[repr(C)]
pub struct PekanCSP {
    _data: [u8; 0],
    _marker: PhantomData<(*mut u8, PhantomPinned)>,
}

#[repr(C)]
pub struct PekanSigner {
    _data: [u8; 0],
    _marker: PhantomData<(*mut u8, PhantomPinned)>,
}

#[repr(C)]
pub struct PekanVerifier {
    _data: [u8; 0],
    _marker: PhantomData<(*mut u8, PhantomPinned)>,
}

#[no_mangle]
pub extern "C" fn pekan_init_csp(
    opts: Options,
    csp_ptr: *mut *const PekanCSP,
    err: &mut ExternError,
) -> ErrorCode {
    env_logger::init();

    let options = unsafe {
        let library = CStr::from_ptr(opts.library).to_string_lossy().into_owned();
        let label = CStr::from_ptr(opts.label).to_string_lossy().into_owned();
        let pin = CStr::from_ptr(opts.pin).to_string_lossy().into_owned();

        pekan::Options {
            library,
            label,
            pin,
            session_cache_size: None,
        }
    };

    let csp = match pekan::CSP::init(options) {
        Ok(csp) => csp,
        Err(error) => {
            *err = ExternError::new_error(
                ffi_support::ErrorCode::new(errors::CONFIG_ERROR),
                error.to_string(),
            );

            return ErrorCode::CommonIOError;
        }
    };

    unsafe {
        *csp_ptr = Box::into_raw(Box::new(csp)) as *const PekanCSP;
    }

    return ErrorCode::Success;
}

#[no_mangle]
pub extern "C" fn pekan_hash(
    csp_ptr: *const PekanCSP,
    msg: &ByteArray,
    algorithm: DigestAlgorithm,
    hash: &mut ByteBuffer,
    err: &mut ExternError,
) -> ErrorCode {
    let message = msg.to_vec();

    let pekan_csp: &pekan::CSP = unsafe { &*(csp_ptr as *const pekan::CSP) };

    match pekan_csp.digest(&message, algorithm) {
        Ok(digest) => {
            *hash = ByteBuffer::from_vec(digest);
            return ErrorCode::Success;
        }
        Err(error) => {
            *err = ExternError::new_error(
                ffi_support::ErrorCode::new(errors::DIGEST_ERROR),
                error.to_string(),
            );

            return ErrorCode::CommonIOError;
        }
    };
}

#[no_mangle]
pub extern "C" fn pekan_new_signer(
    csp_ptr: *const pekan::CSP,
    ski_value: &ByteArray,
    algorithm: DigestAlgorithm,
    signer_ptr: *mut *const PekanSigner,
    err: &mut ExternError,
) -> ErrorCode {
    let ski = ski_value.to_vec();

    let pekan_csp: &pekan::CSP = unsafe { &*(csp_ptr as *const pekan::CSP) };

    let signer = match pekan_csp.new_signer(&ski, algorithm) {
        Ok(signer) => signer,
        Err(error) => {
            *err = ExternError::new_error(
                ffi_support::ErrorCode::new(errors::SIGN_ERROR),
                error.to_string(),
            );

            return ErrorCode::CommonIOError;
        }
    };

    unsafe {
        *signer_ptr = Box::into_raw(signer) as *const PekanSigner;
    }

    return ErrorCode::Success;
}

#[no_mangle]
pub extern "C" fn pekan_signer_sign(
    signer_ptr: *const PekanSigner,
    msg: &ByteArray,
    sign: &mut ByteBuffer,
    err: &mut ExternError,
) -> ErrorCode {
    let signer: &pekan::PKCS11Signer = unsafe { &*(signer_ptr as *const pekan::PKCS11Signer) };

    let message = msg.to_vec();

    match signer.sign(&message) {
        Ok(signature) => {
            *sign = ByteBuffer::from_vec(signature);
            return ErrorCode::Success;
        }
        Err(error) => {
            *err = ExternError::new_error(
                ffi_support::ErrorCode::new(errors::DIGEST_ERROR),
                error.to_string(),
            );

            return ErrorCode::CommonIOError;
        }
    };
}

#[no_mangle]
pub extern "C" fn pekan_new_verifier(
    csp_ptr: *const pekan::CSP,
    ski_value: &ByteArray,
    algorithm: DigestAlgorithm,
    signer_ptr: *mut *const PekanVerifier,
    err: &mut ExternError,
) -> ErrorCode {
    let ski = ski_value.to_vec();

    let pekan_csp: &pekan::CSP = unsafe { &*(csp_ptr as *const pekan::CSP) };

    let verifier = match pekan_csp.new_verifier(&ski, algorithm) {
        Ok(verifier) => verifier,
        Err(error) => {
            *err = ExternError::new_error(
                ffi_support::ErrorCode::new(errors::VERIFY_ERROR),
                error.to_string(),
            );

            return ErrorCode::CommonIOError;
        }
    };

    unsafe {
        *signer_ptr = Box::into_raw(verifier) as *const PekanVerifier;
    }

    return ErrorCode::Success;
}

#[no_mangle]
pub extern "C" fn pekan_verifier_verify(
    verifier_ptr: *const PekanVerifier,
    sign: &ByteArray,
    msg: &ByteArray,
    err: &mut ExternError,
) -> ErrorCode {
    let verifier: &pekan::PKCS11Verifier =
        unsafe { &*(verifier_ptr as *const pekan::PKCS11Verifier) };

    let signature = sign.to_vec();
    let message = msg.to_vec();

    match verifier.verify(&signature, &message) {
        Ok(valid) => {
            if valid {
                return ErrorCode::Success;
            } else {
                return ErrorCode::SignatureInvalid;
            }
        }
        Err(error) => {
            *err = ExternError::new_error(
                ffi_support::ErrorCode::new(errors::VERIFY_ERROR),
                error.to_string(),
            );

            return ErrorCode::SignatureVerifyFailed;
        }
    };
}

#[no_mangle]
pub extern "C" fn pekan_finalize(csp_ptr: *mut PekanCSP, err: &mut ExternError) -> ErrorCode {
    let mut pekan_csp = unsafe { Box::from_raw(csp_ptr as *mut pekan::CSP) };

    let error_code = match pekan_csp.finalize() {
        Ok(()) => ErrorCode::Success,
        Err(error) => {
            *err = ExternError::new_error(
                ffi_support::ErrorCode::new(errors::FINALIZE_ERROR),
                error.to_string(),
            );

            ErrorCode::CommonIOError
        }
    };

    return error_code;
}
